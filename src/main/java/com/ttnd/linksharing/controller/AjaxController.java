/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.springframework.stereotype.Controller
 *  org.springframework.web.bind.annotation.RequestMapping
 *  org.springframework.web.bind.annotation.ResponseBody
 */
package com.ttnd.linksharing.controller;

import java.util.Calendar;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ttnd.linksharing.color.ConsoleColor;
import com.ttnd.linksharing.entities.LinkResource;
import com.ttnd.linksharing.entities.Subscription;
import com.ttnd.linksharing.service.LinkResourceService;
import com.ttnd.linksharing.service.SubscriptionService;
import com.ttnd.linksharing.service.TopicService;
import com.ttnd.linksharing.service.UserService;

@Controller
@RequestMapping(value="/ajax")
public class AjaxController {
    
	@Autowired
	private TopicService topicService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private LinkResourceService linkResourceService;
	
	@RequestMapping(value={"/demo"})
    @ResponseBody
    public String demo() {
        return "This is a demo page";
    }
	
	@RequestMapping(value="savetopic",method=RequestMethod.POST)
	public @ResponseBody String saveTopic(@RequestParam("topicName")String topic,@RequestParam("visibility")String visibility,@RequestParam("username")String username){
		System.out.println(topic+", "+visibility);
		if(!topicService.checkTopic(topic)) {
			topicService.addTopicAndVisibility(topic,visibility,username);
			topicService.addSubscription(topic,username);
			return "success";
		}
		else
			return "Topic Already created by "+topicService.getTopic(topic).getCreatedBy().getFirstName()+" "+topicService.getTopic(topic).getCreatedBy().getLastName();
	}
	
	@RequestMapping(value="gettopicdom",method=RequestMethod.POST)
	public @ResponseBody ModelAndView getTopicDOM(){
		
		return new ModelAndView("topicdom");
		
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="gettopic",method=RequestMethod.GET)
	public @ResponseBody String getTopic(@RequestParam("username")String username){
			System.out.println(ConsoleColor.ANSI_GREEN_BOLD+"Topic request Got"+ConsoleColor.ANSI_RESET);
			List<?> list = subscriptionService.getSubscriptions(username);
			
			JSONObject topicJSON = new JSONObject();
			Subscription subs=null;
			int i=0;
			for(Object l:list){
				String key = ""+ i++ +"";
				subs = (Subscription) l;
				topicJSON.put(key, subs.getTopic().getName());
			}
			System.out.println(ConsoleColor.ANSI_CYAN_BOLD+topicJSON.toJSONString()+ConsoleColor.ANSI_RESET);
			
		return topicJSON.toJSONString();
		
	}
	
	@RequestMapping(value="savelink",method=RequestMethod.POST)
	public @ResponseBody String saveLink(@RequestParam("username")String username,@RequestParam("url")String url,@RequestParam("description")String description,@RequestParam("topic")String topic){
		if(username!=null & url!=null && description!=null) {
			System.out.println(ConsoleColor.ANSI_PURPLE_BOLD+"Data Recieved : "+username+", "+url+", "+description+" ,"+topic+ConsoleColor.ANSI_RESET);
			LinkResource resource = new LinkResource();
			resource.setTopic(topicService.getTopic(topic));
			resource.setUrl(url);
			resource.setDescription(description);
			resource.setCreatedBy(userService.getUser(username));
			resource.setDateCreated(Calendar.getInstance());
			resource.setLastUpdated(Calendar.getInstance());
			
			linkResourceService.saveResource(resource);
			
			/*resource.se*/
			return "Data recieved";
		}
		return "Data Failure";
	}
	
	@RequestMapping(value="savedocument",method=RequestMethod.POST)
	public @ResponseBody String saveDocument(@RequestParam("username")String username,@RequestParam("document")MultipartFile document,@RequestParam("description")String description,@RequestParam("topic")String topic){
		/*if(username!=null & document!=null && description!=null) {
			System.out.println(ConsoleColor.ANSI_PURPLE_BOLD+"Data Recieved : "+username+", "+url+", "+description+" ,"+topic+ConsoleColor.ANSI_RESET);
			LinkResource resource = new LinkResource();
			resource.setTopic(topicService.getTopic(topic));
			resource.setUrl(document);
			resource.setDescription(description);
			resource.setCreatedBy(userService.getUser(username));
			resource.setDateCreated(Calendar.getInstance());
			resource.setLastUpdated(Calendar.getInstance());
			
			linkResourceService.saveResource(resource);
			
			resource.se
			return "Data recieved";
		}*/
		if(username!=null & document!=null && description!=null) {
			System.out.println("Data Recieved Sucessfully");
			return "success";
		}		
		return "Data Failure";
	}
}

