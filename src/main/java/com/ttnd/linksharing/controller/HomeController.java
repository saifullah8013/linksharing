/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  javax.servlet.ServletContext
 *  javax.servlet.http.HttpServletRequest
 *  javax.servlet.http.HttpSession
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.stereotype.Controller
 *  org.springframework.web.bind.annotation.ModelAttribute
 *  org.springframework.web.bind.annotation.PathVariable
 *  org.springframework.web.bind.annotation.RequestMapping
 *  org.springframework.web.bind.annotation.RequestMethod
 *  org.springframework.web.bind.annotation.RequestParam
 *  org.springframework.web.bind.annotation.ResponseBody
 *  org.springframework.web.multipart.MultipartFile
 *  org.springframework.web.servlet.ModelAndView
 */
package com.ttnd.linksharing.controller;

import com.ttnd.linksharing.dto.UserDTO;
import com.ttnd.linksharing.entities.User;
import com.ttnd.linksharing.service.UserService;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/home")
public class HomeController {
	public static final String ANSI_RESET = "\u001b[0m";
	public static final String ANSI_BLACK = "\u001b[30m";
	public static final String ANSI_BLACK_BOLD = "\u001b[30;1m";
	public static final String ANSI_BLACK_BACK = "\u001b[40m";
	public static final String ANSI_RED = "\u001b[31m";
	public static final String ANSI_RED_BOLD = "\u001b[31;1m";
	public static final String ANSI_RED_BACK = "\u001b[41m";
	public static final String ANSI_GREEN = "\u001b[32m";
	public static final String ANSI_GREEN_BOLD = "\u001b[32;1m";
	public static final String ANSI_GREEN_BACK = "\u001b[44m";
	public static final String ANSI_PURPLE = "\u001b[35m";
	public static final String ANSI_PURPLE_BOLD = "\u001b[35;1m";
	public static final String ANSI_PURPLE_BACK = "\u001b[45m";
	public static final String ANSI_CYAN = "\u001b[36m";
	public static final String ANSI_CYAN_BOLD = "\u001b[36;1m";
	public static final String ANSI_CYAN_BACK = "\u001b[46m";
	public static final String ANSI_WHITE = "\u001b[37m";
	public static final String ANSI_WHITE_BOLD = "\u001b[37;1m";
	public static final String ANSI_WHITE_BACK = "\u001b[47m";
	public static final String ANSI_YELLOW = "\u001b[33m";
	public static final String ANSI_YELLOW_BOLD = "\u001b[33m";
	public static final String ANSI_YELLOW_BACK = "\u001b[43m";
	@Autowired
	private UserService userService;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;

	@RequestMapping(value = { "" })
	public ModelAndView showHome(HttpSession session) {
		ModelAndView view = null;
		if (request.getSession().getAttribute("username") != null) {
			view = new ModelAndView("home");
			User user = userService.getUser(request.getSession().getAttribute("username").toString());
			request.getSession().setAttribute("user", user);
			request.getSession().setAttribute("username", request.getSession().getAttribute("username"));
			System.out.println("\u001b[32;1mLogged in Successfully.\u001b[0m");
			view = new ModelAndView("home");
			view.addObject("photo", request.getSession().getAttribute("username") + ".jpeg");
			view.addObject("username", request.getSession().getAttribute("username"));
			view.addObject("fullname",user.getFirstName()+" "+user.getLastName());
		} else {
			System.out.println("Hit made to the home page");
			view = new ModelAndView("view");
			view.addObject("home", "MD Saifullah Khan");
		}

		return view;
	}

	@RequestMapping(value = "/simple")
	@ResponseBody
	public String simple() {
		return "Demo Controller";
	}

	/*
	 * @RequestMapping(value="/{country}/{name}") public ModelAndView
	 * showGreet(@PathVariable(value="name") String
	 * name, @PathVariable(value="country") String country) { ModelAndView view
	 * = new ModelAndView("home"); view.addObject("placeHolder",
	 * (Object)(String.valueOf(name) + " you are from " + country)); return
	 * view; }
	 */

	/*
	 * @RequestMapping(value="phone/{phone}/{name}") public ModelAndView
	 * showPhone(@PathVariable Map<String, String> paths) { ModelAndView view =
	 * new ModelAndView("home"); view.addObject("placeHolder",
	 * (Object)(String.valueOf(paths.get("name")) + " you have a  " +
	 * paths.get("phone"))); return view; }
	 */

	@RequestMapping(value = "/submitform", method = { RequestMethod.POST })
	public ModelAndView submitform(@ModelAttribute(value = "user") UserDTO user,
			@RequestParam(value = "confirmPassword") String confirmPassword,
			@RequestParam(value = "email") String email, @RequestParam(value = "image") MultipartFile file)
					throws IOException {
		if (file == null) {
			System.out.println("\u001b[31;1mFailed to get the file\u001b[0m");
		} else {
			System.out.println("\u001b[32;1mSuccessfully recieved the file \nFile Size : "
					+ (double) file.getSize() / 1000.0 + "kB" + "\u001b[0m");
			String createName = user.getUserName();
			String filePath = String.valueOf(this.makeImageStore()) + createName + "."
					+ file.getContentType().substring(6, file.getContentType().length());
			file.transferTo(new File(filePath));
			System.out.println("\u001b[36;1m" + filePath + "\u001b[0m");
		}
		if (user.getPassword().equals(confirmPassword)) {
			System.out.println("\u001b[32mPassword validated\u001b[0m");
		}
		System.out.print("\u001b[30;1m");
		if (this.userService.saveOrUpdate(user).booleanValue()) {
			System.out.println("\u001b[32;1mNew User added to the database\u001b[0m");
		} else {
			System.out.println("\u001b[31;1mUser already exist in the database\u001b[0m");
		}
		ModelAndView model = new ModelAndView("test");
		return model;
	}

	@RequestMapping(value = "registrationRequest")
	@ResponseBody
	public String registrationRequest(@ModelAttribute UserDTO user, @RequestParam(value = "userName") String username,
			@RequestParam(value = "photo") MultipartFile file) {
		System.out.println("\u001b[32;1mAjax Successfully done.\u001b[0m");
		System.out.println("\u001b[32;1mUserName :" + user.getUserName() + "\u001b[0m");
		System.out.println("\u001b[32;1mEmail ID :" + user.getEmail() + "\u001b[0m");
		System.out.println("\u001b[32;1musername :" + username + "\u001b[0m");
		System.out.println("\u001b[32;1m\u001b[0m");
		if (file == null) {
			System.out.println("\u001b[31;1mFailed to get the file\u001b[0m");
		} else {
			System.out.println("\u001b[32;1mSuccessfully recieved the file\u001b[0m");
		}
		return "true";
	}

	@RequestMapping(value = "login-request", method = { RequestMethod.POST })
	public ModelAndView loginRequest(@RequestParam(value = "userId") String username,
			@RequestParam(value = "password") String password) throws ServletException, IOException {
		ModelAndView view = null;

		System.out.println("\u001b[43m" + username + " tried login to the application" + "\u001b[0m");
		if (!this.userService.loginRequest(username, password).booleanValue()) {
			System.out.println("\u001b[31;1mFailed to Login\u001b[0m");
			return this.errorLogin();
		}
		else{
			User user = userService.getUser(username);
			request.getSession().setAttribute("user", user);
			request.getSession().setAttribute("username", username);
			System.out.println("\u001b[32;1mLogged in Successfully.\u001b[0m");
			view = new ModelAndView("home");
			view.addObject("photo", request.getSession().getAttribute("username") + ".jpeg");
			view.addObject("username", request.getSession().getAttribute("username"));
			view.addObject("fullname",user.getFirstName()+" "+user.getLastName());
		}

		/*
		 * if(request.getSession().getAttribute("username")==null)
		 * request.getSession().setAttribute("username", username); else
		 * System.out.println("Session already created");
		 */

		return view;
	}
	
	@RequestMapping(value="logout",method=RequestMethod.GET)
	public ModelAndView logout(){
		System.out.println("Logout Hit");
		request.getSession().invalidate();
		return new ModelAndView("view");
		
	}

	private ModelAndView errorLogin() {
		if (this.userService != null) {
			System.out.println("autowire done");
		}
		ModelAndView view = new ModelAndView("errorLogin");
		return view;
	}

	private String makeImageStore() {
		String imageStore = this.request.getRealPath("resources/imageStore");
		String realPath = String.valueOf(imageStore) + "/";
		if (!new File(realPath).exists()) {
			new File(realPath).mkdir();
		}
		return realPath;
	}
}
