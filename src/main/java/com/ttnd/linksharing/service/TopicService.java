package com.ttnd.linksharing.service;

import com.ttnd.linksharing.entities.Topic;

public interface TopicService {
	public Boolean checkTopic(String topic);
	
	public Topic getTopic(String topic);

	public void addTopicAndVisibility(String topic, String visibility, String username);

	public void addSubscription(String topic, String username);
}