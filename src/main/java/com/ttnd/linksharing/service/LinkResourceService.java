package com.ttnd.linksharing.service;

import com.ttnd.linksharing.entities.LinkResource;

public interface LinkResourceService {

	public void saveResource(LinkResource resource);
	
}
