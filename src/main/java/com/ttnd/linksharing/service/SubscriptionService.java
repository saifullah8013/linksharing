package com.ttnd.linksharing.service;

import java.util.List;

import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;

public interface SubscriptionService {

	public void subscribe(User getUser, Topic getTopic);

	public List<?> getSubscriptions(String username);

}
