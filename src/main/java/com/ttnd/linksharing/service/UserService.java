/*
 * Decompiled with CFR 0_114.
 */
package com.ttnd.linksharing.service;

import com.ttnd.linksharing.dto.UserDTO;
import com.ttnd.linksharing.entities.User;

public interface UserService {
	public Boolean saveOrUpdate(UserDTO var1);

	public Boolean checkMail(String var1);

	public Boolean loginRequest(String var1, String var2);

	public User getUser(String username);
}
