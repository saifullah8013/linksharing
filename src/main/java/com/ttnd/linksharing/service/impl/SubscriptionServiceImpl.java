package com.ttnd.linksharing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ttnd.linksharing.dao.SubscriptionDAO;
import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;
import com.ttnd.linksharing.service.SubscriptionService;
import com.ttnd.linksharing.service.UserService;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
	
	@Autowired
	private SubscriptionDAO subscriptionDAO;
	@Autowired
	private UserService userService;
	
	public SubscriptionServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void subscribe(User getUser, Topic getTopic) {
		subscriptionDAO.subscribe(getUser,getTopic);
	}

	@Override
	public List<?> getSubscriptions(String username) {
		
		return subscriptionDAO.getSubscriptions(userService.getUser(username));
	}

}
