package com.ttnd.linksharing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ttnd.linksharing.dao.LinkResourceDAO;
import com.ttnd.linksharing.entities.LinkResource;
import com.ttnd.linksharing.service.LinkResourceService;

@Service
public class LinkResourceServiceImpl implements LinkResourceService {
	
	@Autowired
	LinkResourceDAO linkResourceDAO;
	
	public LinkResourceServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void saveResource(LinkResource resource) {
		linkResourceDAO.save(resource);
	}

}
