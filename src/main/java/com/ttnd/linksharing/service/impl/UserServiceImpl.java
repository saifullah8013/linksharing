/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.stereotype.Service
 *  org.springframework.transaction.annotation.Propagation
 *  org.springframework.transaction.annotation.Transactional
 */
package com.ttnd.linksharing.service.impl;

import com.ttnd.linksharing.dao.UserDAO;
import com.ttnd.linksharing.dto.UserDTO;
import com.ttnd.linksharing.entities.User;
import com.ttnd.linksharing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDAO userDAO;

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public Boolean saveOrUpdate(UserDTO userDTO) {
		return this.userDAO.addUser(userDTO);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public Boolean checkMail(String email) {
		return this.userDAO.checkMail(email);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public Boolean loginRequest(String userName, String password) {
		return this.userDAO.validateLogin(userName, password);
	}

	@Override
	public User getUser(String username) {
		// TODO Auto-generated method stub
		return userDAO.getUser(username);
	}
}
