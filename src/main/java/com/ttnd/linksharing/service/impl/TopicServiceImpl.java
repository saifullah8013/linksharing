package com.ttnd.linksharing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ttnd.linksharing.dao.TopicDAO;
import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;
import com.ttnd.linksharing.service.SubscriptionService;
import com.ttnd.linksharing.service.TopicService;
import com.ttnd.linksharing.service.UserService;

@Service
public class TopicServiceImpl implements TopicService {

	@Autowired
	private TopicDAO topicDAO;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SubscriptionService subscriptionService;

	public TopicServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public Boolean checkTopic(String topic) {
		if (topicDAO.getTopic(topic) != null)
			return true;
		return false;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public void addTopicAndVisibility(String topic, String visibility, String username) {
		topicDAO.addTopicAndVisibility(topic,visibility, username);
	}

	@Override
	public Topic getTopic(String topic) {
		
		return topicDAO.getTopic(topic);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public void addSubscription(String topic, String username) {
		Topic getTopic = topicDAO.getTopic(topic);
		User getUser = userService.getUser(username);
		
		subscriptionService.subscribe(getUser,getTopic);
		
		}

}
