package com.ttnd.linksharing.color;

public class ConsoleColor {
	
	public static final String ANSI_RESET = "\u001b[0m";
	public static final String ANSI_BLACK = "\u001b[30m";
	public static final String ANSI_BLACK_BOLD = "\u001b[30;1m";
	public static final String ANSI_BLACK_BACK = "\u001b[40m";
	public static final String ANSI_RED = "\u001b[31m";
	public static final String ANSI_RED_BOLD = "\u001b[31;1m";
	public static final String ANSI_RED_BACK = "\u001b[41m";
	public static final String ANSI_GREEN = "\u001b[32m";
	public static final String ANSI_GREEN_BOLD = "\u001b[32;1m";
	public static final String ANSI_GREEN_BACK = "\u001b[44m";
	public static final String ANSI_PURPLE = "\u001b[35m";
	public static final String ANSI_PURPLE_BOLD = "\u001b[35;1m";
	public static final String ANSI_PURPLE_BACK = "\u001b[45m";
	public static final String ANSI_CYAN = "\u001b[36m";
	public static final String ANSI_CYAN_BOLD = "\u001b[36;1m";
	public static final String ANSI_CYAN_BACK = "\u001b[46m";
	public static final String ANSI_WHITE = "\u001b[37m";
	public static final String ANSI_WHITE_BOLD = "\u001b[37;1m";
	public static final String ANSI_WHITE_BACK = "\u001b[47m";
	public static final String ANSI_YELLOW = "\u001b[33m";
	public static final String ANSI_YELLOW_BOLD = "\u001b[33m";
	public static final String ANSI_YELLOW_BACK = "\u001b[43m";

	public ConsoleColor() {
		// TODO Auto-generated constructor stub
	}

}
