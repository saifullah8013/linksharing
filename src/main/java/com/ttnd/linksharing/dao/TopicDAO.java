package com.ttnd.linksharing.dao;

import com.ttnd.linksharing.entities.Topic;

public interface TopicDAO {

	public Topic getTopic(String topic);

	public void addTopicAndVisibility(String topic, String visibility, String username);
	
}
