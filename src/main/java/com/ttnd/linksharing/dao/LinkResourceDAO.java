package com.ttnd.linksharing.dao;

import com.ttnd.linksharing.entities.LinkResource;

public interface LinkResourceDAO {

	void save(LinkResource resource);

}
