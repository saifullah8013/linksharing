package com.ttnd.linksharing.dao;

import java.util.List;

import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;

public interface SubscriptionDAO {

	void subscribe(User getUser, Topic getTopic);

	List<?> getSubscriptions(User user);

}
