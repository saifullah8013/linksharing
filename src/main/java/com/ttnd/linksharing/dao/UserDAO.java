/*
 * Decompiled with CFR 0_114.
 */
package com.ttnd.linksharing.dao;

import com.ttnd.linksharing.dto.UserDTO;
import com.ttnd.linksharing.entities.User;

public interface UserDAO {
    public Boolean addUser(UserDTO var1);

    public UserDTO getUser(Long var1);

    public User getUser(String var1);

    public void deleteUser(Long var1);

    public void updateUser();

    public Boolean checkMail(String var1);

    public Boolean validateLogin(String var1, String var2);
}

