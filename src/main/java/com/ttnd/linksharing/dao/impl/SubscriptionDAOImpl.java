package com.ttnd.linksharing.dao.impl;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ttnd.linksharing.dao.SubscriptionDAO;
import com.ttnd.linksharing.entities.Subscription;
import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;

@Repository
public class SubscriptionDAOImpl implements SubscriptionDAO {

	@PersistenceContext
	private EntityManager manager;

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

	public SubscriptionDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public void subscribe(User getUser, Topic getTopic) {
		Subscription subscription = new Subscription();
		subscription.setTopic(getTopic);
		subscription.setUser(getUser);
		subscription.setDateCreated(Calendar.getInstance());

		if (manager.createQuery("select s from Subscription s where topic = :topic and user = :user")
				.setParameter("topic", getTopic).setParameter("user", getUser).getResultList().isEmpty()) {
			
			manager.persist(subscription);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public List<?> getSubscriptions(User user) {
		return manager.createQuery("select s from Subscription s where user= :user").setParameter("user", user).getResultList();
	}

}
