/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  javax.persistence.EntityManager
 *  javax.persistence.PersistenceContext
 *  javax.persistence.Query
 *  org.springframework.stereotype.Repository
 *  org.springframework.transaction.annotation.Propagation
 *  org.springframework.transaction.annotation.Transactional
 */
package com.ttnd.linksharing.dao.impl;

import com.ttnd.linksharing.dao.UserDAO;
import com.ttnd.linksharing.dto.UserDTO;
import com.ttnd.linksharing.entities.User;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserDAOImpl
implements UserDAO {
	@PersistenceContext
	private EntityManager manager;

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }

    @Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
    @Override
    public Boolean addUser(UserDTO userDTO) {
        User user = new User();
        user.setUserName(userDTO.getUserName());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setPhoto(userDTO.getPhoto());
        user.setDateCreated(Calendar.getInstance());
        user.setLastUpdated(Calendar.getInstance());
        if (this.getUser(userDTO.getUserName())!=null) {
            return false;
        }
        this.manager.persist(user);
        return true;
    }

    @Override
    public UserDTO getUser(Long id) {
        return null;
    }

    @Override
    public User getUser(String userName) {
        Query query = this.manager.createQuery("select u from User u where userName = :name");
        query.setParameter("name", userName);
        List<?> result = query.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return (User) result.get(0);
    }

    @Override
    public void deleteUser(Long id) {
    }

    @Override
    public void updateUser() {
    }

    @Override
    public Boolean checkMail(String mail) {
        Query query = this.manager.createQuery("select email from User");
        if (!query.getResultList().isEmpty()) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean validateLogin(String userName, String password) {
        Query query = this.manager.createQuery("select u from User u where userName = :username and password = :password");
        query.setParameter("username", (Object)userName).setParameter("password", password);
        if (!query.getResultList().isEmpty()) {
            return true;
        }
        return false;
    }
}

