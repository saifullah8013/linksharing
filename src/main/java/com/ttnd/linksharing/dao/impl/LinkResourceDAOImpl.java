package com.ttnd.linksharing.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ttnd.linksharing.dao.LinkResourceDAO;
import com.ttnd.linksharing.entities.LinkResource;

@Repository
public class LinkResourceDAOImpl implements LinkResourceDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public void setManager(EntityManager manager){
		this.manager = manager;
	}

	public LinkResourceDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	@Override
	public void save(LinkResource resource) {		
		manager.persist(resource);
	}

}
