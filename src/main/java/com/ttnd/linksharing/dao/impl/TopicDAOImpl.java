package com.ttnd.linksharing.dao.impl;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ttnd.linksharing.dao.TopicDAO;
import com.ttnd.linksharing.dao.UserDAO;
import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;
import com.ttnd.linksharing.entities.Visibility;

@Repository
public class TopicDAOImpl implements TopicDAO {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	UserDAO userDAO;

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

	public TopicDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Topic getTopic(String topic) {
		List<?> list = manager.createQuery("select t from Topic as t where t.name = :topic").setParameter("topic", topic).getResultList();
		if(list.isEmpty())
			return null;
		return (Topic) list.get(0);
	}

	@Override
	public void addTopicAndVisibility(String topic, String visibility, String username) {
		Visibility visible = Visibility.PUBLIC;
		if(!visible.getVisibility().equals(visibility))
			visible = Visibility.PRIVATE;
		User createdBy;
		createdBy = userDAO.getUser(username);
		manager.persist(new Topic(topic, createdBy, Calendar.getInstance(), Calendar.getInstance(), visible));
	}



}
