/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.JoinColumn
 *  javax.persistence.ManyToOne
 *  javax.persistence.Table
 *  javax.persistence.Temporal
 *  javax.persistence.TemporalType
 */
package com.ttnd.linksharing.entities;

import com.ttnd.linksharing.entities.Seriousness;
import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;
import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="subscription")
public class Subscription
implements Serializable {
    private static final long serialVersionUID = 3704617422006354682L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name="topic")
    private Topic topic;
    @ManyToOne
    @JoinColumn(name="user")
    private User user;
    @Enumerated(EnumType.STRING)
    private Seriousness seriousness;
    
    @Column(name="date_created")
    @Temporal(value=TemporalType.DATE)
    private Calendar dateCreated;

    public Subscription(Topic topic, User user, Seriousness seriousness, Calendar dateCreated) {
        this.topic = topic;
        this.user = user;
        this.seriousness = seriousness;
        this.dateCreated = dateCreated;
    }

    public Subscription() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public Topic getTopic() {
        return this.topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Seriousness getSeriousness() {
        return this.seriousness;
    }

    public void setSeriousness(Seriousness seriousness) {
        this.seriousness = seriousness;
    }

    public Calendar getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(Calendar dateCreated) {
        this.dateCreated = dateCreated;
    }

    public static long getSerialversionuid() {
        return 3704617422006354682L;
    }
}

