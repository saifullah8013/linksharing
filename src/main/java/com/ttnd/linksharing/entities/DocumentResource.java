/*
 * Decompiled with CFR 0_114.
 */
package com.ttnd.linksharing.entities;

import com.ttnd.linksharing.entities.Resource;
import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;
import java.util.Calendar;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="documentresource")
@DiscriminatorValue("document")
public class DocumentResource
extends Resource {
    private static final long serialVersionUID = -8542287892351653214L;
    private String filePath;

    public DocumentResource() {
    }

    public DocumentResource(String description, User createdBy, Topic topic, Calendar dateCreated, Calendar lastUpdated, String filePath) {
        super(description, createdBy, topic, dateCreated, lastUpdated);
        this.filePath = filePath;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public static long getSerialversionuid() {
        return -8542287892351653214L;
    }
}

