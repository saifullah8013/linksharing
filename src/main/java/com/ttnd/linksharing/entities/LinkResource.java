/*
 * Decompiled with CFR 0_114.
 */
package com.ttnd.linksharing.entities;

import com.ttnd.linksharing.entities.Resource;
import com.ttnd.linksharing.entities.Topic;
import com.ttnd.linksharing.entities.User;
import java.util.Calendar;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="linkresource")
@DiscriminatorValue("link")
public class LinkResource
extends Resource {
    private static final long serialVersionUID = 6981888756177542689L;
    private String url;

    public LinkResource(String description, User createdBy, Topic topic, Calendar dateCreated, Calendar lastUpdated, String url) {
        super(description, createdBy, topic, dateCreated, lastUpdated);
        this.url = url;
    }

    public LinkResource() {
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static long getSerialversionuid() {
        return 6981888756177542689L;
    }
}

