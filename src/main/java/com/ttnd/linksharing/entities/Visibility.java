/*
 * Decompiled with CFR 0_114.
 */
package com.ttnd.linksharing.entities;

public enum Visibility {
    PUBLIC("public"),
    PRIVATE("private");
    
    private final String visibility;

    private Visibility(String visibility) {
        this.visibility = visibility;
    }
    
    public String getVisibility() {
        return this.visibility;
    }
}

