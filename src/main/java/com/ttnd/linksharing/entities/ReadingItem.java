/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.JoinColumn
 *  javax.persistence.ManyToOne
 *  javax.persistence.Table
 */
package com.ttnd.linksharing.entities;

import com.ttnd.linksharing.entities.Resource;
import com.ttnd.linksharing.entities.User;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="reading_item")
public class ReadingItem
implements Serializable {
    private static final long serialVersionUID = -7154197500455685917L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name="resource_id")
    private Resource resource;
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;
    private Boolean isRead;

    public ReadingItem(Resource resource, User user, Boolean isRead) {
        this.resource = resource;
        this.user = user;
        this.isRead = isRead;
    }

    public ReadingItem() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Resource getResource() {
        return this.resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getIsRead() {
        return this.isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public static long getSerialversionuid() {
        return -7154197500455685917L;
    }
}

