/*
 * Decompiled with CFR 0_114.
 */
package com.ttnd.linksharing.entities;

public enum Seriousness {
    SERIOUS("serious"),
    VERYSERIOUS("veryserious"),
    CASUAL("casual");
    
    private final String seriousness;
    
    private Seriousness(String seriousness) {
        this.seriousness = seriousness;
    }

    public String getSeriousness() {
        return this.seriousness;
    }
}

