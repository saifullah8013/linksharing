<%@page import="org.hibernate.Session"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML>
<html>
    <head>
        <title>HOME</title>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/jquery.min.js"></script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3.3.6-dist/css/bootstrap.min.css" type="text/css">
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <style>
            #navbar{
                border-radius: 15px;
                width: 90%;
                margin-left: 5%;
            }
            #navbar a{
                margin-left: 5px;
                font-size: 20px;
                text-decoration-style: none;
            }
            #search{
                //float: right;
                display: inline-block;
                background: white;
                border-radius: 10px;
                border: 2px solid black;
            }
            #search input{
                padding: 2px;
                background: white;
                border: none;
            }
            #pops,#pops button{
                display: inline;
                margin-left: 0px;
                border: none;
            }
            #pops span{
                margin-left: 1px;
                font-size: 20px;
                text-align: center;
            }
            span {
                font-size: 14px;
                //background: white;
                text-align: center;
            }
            #remov{
                color: white;
                background: grey;
                border-radius: 50%;
                border: 1px solid grey;
            }
            #user span{
                width: 100%;
                padding-top: 3px;
                margin-bottom: -2px;
                font-size: 50px;
                border: 2px solid grey;
                align-content: red;
            }
            .link1{
                margin-left: 5px;
            }
            #userImage{
            border: 1px solid green;
            border-radius:50%;
            width: 40px;
            height: 40px;}
            
            .userImage{
            border-radius: 8%;}
            
            
        </style>
    </head>
    <body>
      
      <!-- ALL THE MODAL HERE -->
          <!-- Modal SEND INVITATION -->
            <div id="sendInvitation" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header well">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send Invitation</h4>
                  </div>
                  <div class="modal-body">
                   <!-- FORM GOES HERE-->
                    <form action="#" class="form-horizontal">
                        <div class="form-group">
                            <label for="email" class="control-label col-xs-2 col-xs-offset-1">Email *</label>
                            <div class="col-xs-8">
                                <input type="email" id="email" placeholder="Enter Email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="topic" class="control-label col-xs-2 col-xs-offset-1">Topic *</label>
                            <div class="col-xs-8">
                                 <div class="dropdown">
                                  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Topic
                                  <span class="caret"></span></button>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                  </ul>
                                 </div>
                            </div>
                        </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button id="sendInvite" type="button" class="btn btn-success">Invite</button>
                    <button id="closeInvite" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </div>
                </div>

              </div>
            </div>
            
                    <!-- Modal SHARE DOCUMENT -->
        <div id="shareDocument" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
              </div>
              <div class="modal-body">
                <form id="documentform" action="#" class="form-horizontal" enctype="multipart/form-data">
                		<div class="form-group">
                            <div class="col-xs-8">
                                <input type="hidden" id="sharedocumentusername" name="username" class="form-control" value="<% out.write(session.getAttribute("username").toString()); %>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="text" class="control-label col-xs-2 col-xs-offset-1">Document *</label>
                            <div class="col-xs-5">
                                <input name="document" type="file" id="sharedocument" class="form-control">
                            </div>
                            <div class="col-xs-3">
                                <input type="button" id="browse" value="Browse" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc" class="control-label col-xs-2 col-xs-offset-1">Description</label>
                            <div class="col-xs-8">
                                <textarea name="description" id="sharedocumentdesc" class="form-control" row="4" placeholder="Add your description here"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="topic" class="control-label col-xs-2 col-xs-offset-1">Topic *</label>
                            <div class="col-xs-8">
                                 <div class="dropdown">
                                  <button id="sharedocumenttopic" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span>Topic</span>
                                  <span class="caret"></span></button>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                  </ul>
                                 </div>
                            </div>
                        </div>
                    </form>
              </div>
              <div class="modal-footer">
                <button id="saveDocument" type="button" class="btn btn-success">Share</button>
                <button id="closeDocument" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </div>

          </div>
        </div>
     
     
             <!-- Modal SHARE LINK -->
        <div id="shareLink" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Share Link</h4>
              </div>
              <div class="modal-body">
                <form action="#" class="form-horizontal">
                        <div class="form-group">
                            <label for="text" class="control-label col-xs-2 col-xs-offset-1">Link *</label>
                            <div class="col-xs-8">
                                <input type="email" id="sharelinkemail" placeholder="Enter Email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc" class="control-label col-xs-2 col-xs-offset-1">Description</label>
                            <div class="col-xs-8">
                                <textarea id="sharelinkdesc" class="form-control" row="4" placeholder="Add your description here"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="topic" class="control-label col-xs-2 col-xs-offset-1">Topic *</label>
                            <div class="col-xs-8">
                                 <div class="dropdown">
                                  <button id="sharelinktopic" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span>Topic</span>
                                  <span class="caret"></span></button>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                  </ul>
                                 </div>
                            </div>
                        </div>
                    </form>
              </div>
              <div class="modal-footer">
                <button id="saveLink" type="button" class="btn btn-success">Share</button>
                <button id="closeLink" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </div>

          </div>
        </div>
        
            <!-- Modal CREATE TOPIC -->
        <div id="createTopic" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header well">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Topic</h4>
              </div>
              <div class="modal-body">
                <form action="#" class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="control-label col-xs-2 col-xs-offset-1">Name *</label>
                            <div class="col-xs-8">
                                <input id="topicName" name="topicName" type="text" id="name" placeholder="Enter Topic Name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="topic" class="control-label col-xs-2 col-xs-offset-1">Visiblity *</label>
                            <div class="col-xs-8">
                                 <div class="dropdown">
                                  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span>Public</span>
                                  <span class="caret"></span></button>
                                  <ul class="dropdown-menu">
                                    <li class="selected"><a href="#">Public</a></li>
                                    <li><a href="#">Private</a></li>
                                  </ul>
                                 </div>
                            </div>
                        </div>
                    </form>
              </div>
              <div class="modal-footer">
               <button id="saveTopic" type="button" class="btn btn-success">Save</button>
                <button id="closeTopic" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </div>

          </div>
        </div>
      <!-- ALL THE MODAL HERE -->
       <!-- THIS IS NAVBAR SECTION -->
        <div class="row">
             <div class="col-sm-12 well" id="navbar">
              <div class="row">
               <div class="col-sm-2"><button data-toggle="modal" data-target="#shareLink" class="btn btn-default well" style="border:0px"><ins style="color:blue">Link Sharing</ins></button></div>
               <div class="col-sm-2"></div>
               <div class="col-sm-8">
                  <div class="row">
                   <div class="col-xs-6"><div id="search"><span class="glyphicon glyphicon-search"></span><input type="search" /><span class="glyphicon glyphicon-remove" id="remov"></span></div>
                   <div id="pops">
                   <button data-toggle="modal" data-target="#createTopic"><span class="glyphicon glyphicon-comment"></span></button>
                   <button data-toggle="modal" data-target="#sendInvitation"><span class="glyphicon glyphicon-envelope"></span></button>
                   <button data-toggle="modal" data-target="#shareLink"><span class="glyphicon glyphicon-link"></span></button>
                   <button data-toggle="modal" data-target="#shareDocument"><span class="glyphicon glyphicon-file"></span></button>
                   </div>
                   </div>
                   <div class="col-xs-6" ><img id="userImage" alt="No idea" src="${pageContext.request.contextPath }/resources/imageStore/${photo}" /><strong id="username" style="padding-left: 2px">${fullname}</strong><div class="dropdown" style="display:inline-block;margin-left:10px">
                                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Serious
                                          <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Profile</a></li>
                                                <li><a href="#">Users</a></li>
                                                <li><a href="#">Topics</a></li>
                                                <li><a href="#">Posts</a></li>
                                                <li><a href="logout">Logout</a></li>
                                              </ul>
                                     </div></div>
                   </div>
               </div>
              </div>
           </div>
        </div>
        <!-- NAV BAR ENDS HERE -->
        <div class="container">
            <!-- MAIN CONTENT -->
            <div class="row">
               <!-- LEFT SIDE CONTENT -->
                <div class="col-xs-5 container">
                    <div class="panel panel-default" style="border:2px solid black;border-radius:10px">
                        <div class="panel-body">
                        <div class="row">
                            <div class="container col-sm-3" id="user"><img class="userImage" width="80px" height="60px" alt="No idea" src="${pageContext.request.contextPath }/resources/imageStore/${photo}" /></div>
                            <div class="container col-sm-9">
                            <div class="row">
                               <p style="font-size:20px"><strong>${fullname}</strong></p>
                               <span class="text-muted">@${username}</span>
                            </div>
                                <div class="row">
                                   <br>
                                    <div class="col-xs-5"><span class="text-muted">Subscriptions</span><br><span style="color:blue">50</span></div>
                                    <div class="col-xs-5"><span class="text-muted">Topics</span><br><span style="color:blue">30</span></div>
                                    <!--<div class="col-xs-2"></div>-->
                                </div>
                            <div>
                            
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="panel panel-default">
                       <div class="panel-heading">
                           Subscriptions
                       </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="container col-sm-3" id="user"><img class="userImage" width="80px" height="60px" alt="No idea" src="${pageContext.request.contextPath }/resources/imageStore/${photo}" /></div>
                            <div class="container col-sm-9">
                            <div class="row">
                               <p style="font-size:20px"><strong>${fullname}</strong></p>
                               <!--<span class="text-muted">@${username}</span>-->
                            </div>
                                <div class="row">
                                   <br>
                                   <div class="col-xs-4"><span class="text-muted">@${username}</span><br><span><ins><a href="#">Unsubscribe</a></ins></span></div>
                                    <div class="col-xs-5"><span class="text-muted">Subscriptions</span><br><span style="color:blue">50</span></div>
                                    <div class="col-xs-3"><span class="text-muted">Topics</span><br><span style="color:blue">30</span></div>
                                    <!--<div class="col-xs-2"></div>-->
                                </div>
                                <br>
                            <div class="row">
                                <div class="col-xs-4">
                                     <div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Serious
                                          <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                
                                              </ul>
                                     </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Private
                                          <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                
                                              </ul>
                                     </div>
                                </div>
                                <div class="col-xs-4">
                                    <span class="glyphicon glyphicon-envelope" style="font-size:25px"></span>
                                    <span class="glyphicon glyphicon-file" style="font-size:25px"></span>
                                    <span class="glyphicon glyphicon-trash" style="font-size:25px"></span>
                                </div>                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <hr>
                        <div class="panel-body">
                        <div class="row">
                            <div class="container col-sm-3" id="user"><img class="userImage" width="80px" height="60px" alt="No idea" src="${pageContext.request.contextPath }/resources/imageStore/${photo}" /></div>
                            <div class="container col-sm-9">
                            <div class="row">
                               <p style="font-size:20px"><strong>${fullname}</strong></p>
                               <!--<span class="text-muted">@${username}</span>-->
                            </div>
                                <div class="row">
                                   <br>
                                   <div class="col-xs-4"><span class="text-muted">@${username}</span><br><span><ins><a href="#">Unsubscribe</a></ins></span></div>
                                    <div class="col-xs-5"><span class="text-muted">Subscriptions</span><br><span style="color:blue">50</span></div>
                                    <div class="col-xs-3"><span class="text-muted">Topics</span><br><span style="color:blue">30</span></div>
                                    <!--<div class="col-xs-2"></div>-->
                                </div>
                                <br>
                            <div class="row">
                                <div class="col-xs-3 col-xs-offset-6">
                                     <div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Serious
                                          <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                
                                              </ul>
                                     </div>
                                </div>
                                <div class="col-xs-2 col-xs-offset-1">
                                    <span class="glyphicon glyphicon-envelope" style="font-size:30px"></span>
                                </div>                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <div class="panel-footer">
                            
                        </div>
                    </div>
                    <div class="panel panel-default">
                       <div class="panel-heading">
                           Trending topics
                       </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="container col-sm-3" id="user"><img class="userImage" width="80px" height="60px" alt="No idea" src="${pageContext.request.contextPath }/resources/imageStore/${photo}" /></div>
                            <div class="container col-sm-9">
                            <div class="row">
                                   <br>
                                   <div class="col-xs-4"><span class="text-muted">@${username}</span><br><span><ins><a href="#">Unsubscribe</a></ins></span></div>
                                    <div class="col-xs-5"><span class="text-muted">Subscriptions</span><br><span style="color:blue">50</span></div>
                                    <div class="col-xs-3"><span class="text-muted">Topics</span><br><span style="color:blue">30</span></div>
                                    <!--<div class="col-xs-2"></div>-->
                                </div>
                            <div>
                            
                            </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="panel-body">
                        <div class="row">
                            <div class="container col-sm-3" id="user"><img class="userImage" width="80px" height="60px" alt="No idea" src="${pageContext.request.contextPath }/resources/imageStore/${photo}" /></div>
                            <div class="container col-sm-9">
                            <div class="row">
                                   <br>
                                   <div class="row">
                                       <div class="col-xs-5"><input type="text" class="form-control" placeholder="Grails"></div>
                                       <div class="col-xs-3"><button class="btn btn-default">Save</button></div>
                                       <div class="col-xs-4"><button class="btn btn-default">Cancel</button></div>
                                   </div>
                                   <br>
                                   <div class="col-xs-4"><span class="text-muted">@${username}</span><br><span><ins><a href="#">Unsubscribe</a></ins></span></div>
                                    <div class="col-xs-5"><span class="text-muted">Subscriptions</span><br><span style="color:blue">50</span></div>
                                    <div class="col-xs-3"><span class="text-muted">Topics</span><br><span style="color:blue">30</span></div>
                                    <!--<div class="col-xs-2"></div>-->
                                </div><br>
                            </div>
                                <div class="col-xs-4 col-xs-offset-1">
                                     <div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Serious
                                          <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                
                                              </ul>
                                     </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Private
                                          <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                
                                              </ul>
                                     </div>
                                </div>
                                <div class="col-xs-3">
                                    <span class="glyphicon glyphicon-envelope" style="font-size:25px"></span>
                                    <span class="glyphicon glyphicon-file" style="font-size:25px"></span>
                                    <span class="glyphicon glyphicon-trash" style="font-size:25px"></span>
                                </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
                <!-- RIGHT SIDE CONTENT -->
                <div class="col-xs-7 container">
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        Recent Shares
                    </div>
                    <!-- RECENT SHARES PANEL BODY -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="container col-sm-2" id="user"><img class="userImage" width="80px" height="60px" alt="No idea" src="${pageContext.request.contextPath }/resources/imageStore/${photo}" /></div>
                            <div class="container col-sm-10">
                            <div class="row">
                               <div class="col-xs-4 s">${fullname}</div>
                               <div class="col-xs-6 s"><span class="text-muted">@${username} 10 min</span></div>
                               <div class="col-xs-2 s" style="color:blue">Grails</div>
                            </div>
                                <div><p>safasfas ipsum sasddolor sit amet, consectetur adipisicing elit. Rerum inventore voluptas quibusdam. Eaque doloremque ducimus nam, animi nemo porro obcaecati distinctio mollitia at aliquam sunt asperiores quidem consectetur! Repellat, ea.</p></div>
                            <div>
                            <div class="row">
                                <div class="col-xs-3 social">
                                <img src="${pageContext.request.contextPath}/resources/bootstrap-3.3.6-dist/fb.png" alt="" width="25px" height="25px">
                                <img src="${pageContext.request.contextPath}/resources/bootstrap-3.3.6-dist/g+.png" alt="" width="25px" height="25px">
                                <img src="${pageContext.request.contextPath}/resources/bootstrap-3.3.6-dist/tweet.png" alt="" width="25px" height="25px">
                                </div>
                                <div class="col-xs-9" style="text-align:right">
                                <ins><a class="link1" href="#">Download</a></ins>
                                <ins><a class="link1" href="#">View full site</a></ins>
                                <ins><a class="link1" href="#">Mark as read</a></ins>
                                <ins><a class="link1" href="#">View post</a></ins>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <!-- PANEL BODY ENDS HERE -->
                    
                    <!-- RECENT SHARES PANEL 2nd BODY -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="container col-sm-2" id="user"><img class="userImage" width="80px" height="60px" alt="No idea" src="${pageContext.request.contextPath }/resources/imageStore/${photo}" /></div>
                            <div class="container col-sm-10">
                            <div class="row">
                               <div class="col-xs-4 s">${fullname}</div>
                               <div class="col-xs-6 s"><span class="text-muted">@${username} 10 min</span></div>
                               <div class="col-xs-2 s" style="color:blue">Grails</div>
                            </div>
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum inventore voluptas quibusdam. Eaque doloremque ducimus nam, animi nemo porro obcaecati distinctio mollitia at aliquam sunt asperiores quidem consectetur! Repellat, ea.</p></div>
                            <div><div class="row">
                                <div class="col-xs-3">
                                    <img src="${pageContext.request.contextPath}/resources/bootstrap-3.3.6-dist/fb.png" alt="" width="25px" height="25px">
                                    <img src="${pageContext.request.contextPath}/resources/bootstrap-3.3.6-dist/g+.png" alt="" width="25px" height="25px">
                                    <img src="${pageContext.request.contextPath}/resources/bootstrap-3.3.6-dist/tweet.png" alt="" width="25px" height="25px">
                                </div>
                                <div class="col-xs-9" style="text-align:right">
                                    <ins><a class="link1" href="#">Download</a></ins>
                                    <ins><a class="link1" href="#">View full site</a></ins>
                                    <ins><a class="link1" href="#">Mark as read</a></ins>
                                    <ins><a class="link1" href="#">View post</a></ins>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- PANEL 2nd BODY ENDS HERE -->
                   </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
       	$(document).ready(function(){
       		var username=$("#username").text();
       		console.log("ok jquery is working fine in eclipse. Eclipse is great to work.");
       		
       		/* CODE RELATED TO SHARING LINK GOES HERE */
       		
       		//DTO for carrying informatin related to sharing link
       		var linkResourceDTO = {
       				username:"<% out.write(session.getAttribute("username").toString()); %>",
       				url:"",
       				description:"",
       				topic:""
       				}
       		
       		//DTO for carrying informatin related to sharing document
       		var documentResourceDTO = {
       				username:"<% out.write(session.getAttribute("username").toString()); %>",
       				document:{},
       				description:"",
       				topic:""
       				}
       		
       		
       		$("#sharelinkemail").change(function(){
       			console.log($(this).val());
       			linkResourceDTO.url=$(this).val();
       		});
       		$("#sharelinkdesc").change(function(){
       			console.log($(this).val());
       			linkResourceDTO.description = $(this).val();
       		});
       		$("#sharedocumentdesc").change(function(){
       			console.log($(this).val());
       			documentResourceDTO.description = $(this).val();
       		});
       		$("#sharedocument").change(function(){
       			console.log($(this).val());
       			documentResourceDTO.document = $(this);
       		});
       		//Unknown
       		$("#sharelinktopic").next().find("li").click(function(){
       			console.log("topic clicked is"+$(this).find("a").text());
       		});
       		
       		//Sending link saving request
       		$("#saveLink").click(function() {
				console.log("Saved");
				$.ajax({
					url:"../ajax/savelink",
					type:"POST",
					data:linkResourceDTO,
					success:function(response){
						console.log(response);
					}
				});
				/* $("#closeLink").trigger("click"); */
				console.log("Pop-up closed")
			});
       		
       		//Sending Document Saving Request
       		$("#saveDocument").click(function() {
       			var formData = new FormData(document.querySelector('#documentform'));
       				formData.append('topic',documentResourceDTO.topic);
       			/* formData.append('topic',documentResourceDTO.topic);
       			formData.append('username',documentResourceDTO.username);
       			formData.append('description',documentResourceDTO.description);
       			formData.append('document',documentResourceDTO.document.val()); */
       			
       			console.log(formData);
				console.log("Saved");
				$.ajax({
					url:"../ajax/savedocument",
					type:"POST",
					data:formData,
					cache:false,
					contentType:false,
					processData:false,
					success:function(response){
						console.log(response);
					}
				});
				/* $("#closeLink").trigger("click"); */
				console.log("Pop-up closed")
			});
       		
       		//Topic Dropdown Work for link resource
       		$("#sharelinktopic").click(function(){       				
       				$.ajax({
       					url:"../ajax/gettopicdom",
       					type:"POST",
       					success:function(dom){
       						console.log(dom);
       						//Logic of getting the topic and rendering with the dom
       						$.ajax({
       							url:"../ajax/gettopic",
       							data:{username:"<% out.write(session.getAttribute("username").toString()); %>"},
       							success:function(topicJSON){
       								console.log(topicJSON);
       								var JSONObject = JSON.parse(topicJSON);
       								console.log(JSONObject);
       						/* changed */		$("#sharelinktopic").next().html("");
       								for(topic in JSONObject){
       									console.log(JSONObject[topic]);
       									console.log($(this).toString())
       									/* changed */	$("#sharelinktopic").next().append(insertIntoDOM(dom, JSONObject[topic]));
       								}
       								/* changed */	$("#sharelinktopic").next().find("li").click(function(){
       									console.log($(this).parent());
       									$(this).parent().prev().html("<span>"+$(this).text()+"</span>"+"<span class=\"caret\"></span>");
       									linkResourceDTO.topic=$(this).text().toString();
       									});
       							}
       						});      						
       					}       					
       				});
       			
       			
       		});
       		
       		
       	//Topic Dropdown Work for document resource
       		$("#sharedocumenttopic").click(function(){       				
       				$.ajax({
       					url:"../ajax/gettopicdom",
       					type:"POST",
       					success:function(dom){
       						console.log(dom);
       						//Logic of getting the topic and rendering with the dom
       						$.ajax({
       							url:"../ajax/gettopic",
       							data:{username:"<% out.write(session.getAttribute("username").toString()); %>"},
       							success:function(topicJSON){
       								console.log(topicJSON);
       								var JSONObject = JSON.parse(topicJSON);
       								console.log(JSONObject);
       						/* changed */		$("#sharedocumenttopic").next().html("");
       								for(topic in JSONObject){
       									console.log(JSONObject[topic]);
       									console.log($(this).toString())
       									/* changed */	$("#sharedocumenttopic").next().append(insertIntoDOM(dom, JSONObject[topic]));
       								}
       								/* changed */	$("#sharedocumenttopic").next().find("li").click(function(){
       									console.log($(this).parent());
       									$(this).parent().prev().html("<span>"+$(this).text()+"</span>"+"<span class=\"caret\"></span>");
       									documentResourceDTO.topic=$(this).text().toString();
       									console.log(documentResourceDTO);
       									});
       							}
       						});      						
       					}       					
       				});
       			
       			
       		});
       		      		
       		/* CODES RELATED TO CREATING TOPICS IS HERE */
       		$("#saveTopic").click(function() {
				console.log("Save topic button is clicked");
				$("#createTopic form").next().detach();
				$.ajax({
					url:"../ajax/savetopic",
					data:{
						visibility:$.trim($("#createTopic .selected").text().toString().toLowerCase()),
						topicName:$.trim($("#topicName").val().toString()),
						username:"<% out.write(session.getAttribute("username").toString()); %>"
					},
					type:"POST",
					success:function(response){
						
						if(response==="success"){
							$("#createTopic form").next().detach();
							$("#createTopic form").after("<p>"+"Topics Addedd Successfully"+"</p>");
							window.setTimeout(function(){
								$("#createTopic form").next().detach();
								$("#closeTopic").trigger("click");},1500)							
						}
						else{
							$("#createTopic form").next().detach();
							$("#createTopic form").after("<p>"+response.toString()+"</p>");
						}
					}
				});
			});
       		$("#saveDocument").click(function() {
				console.log("Saved");
				$("#closeDocument").trigger("click");
				console.log("Pop-up closed")
			});
       		
       		$("#sendInvite").click(function() {
				console.log("Saved");
				$("#closeInvite").trigger("click");
				console.log("Pop-up closed")
			});
       		
       		$("#createTopic li").click(function(){$("#createTopic li").removeClass("selected");$(this).addClass("selected");$("#createTopic .selected").closest("div").find("button span:first").text($("#createTopic .selected").text())})
       		
       	});
       	
       	function insertIntoDOM(dom,value){
       		var augmentedDOM = dom.replace("replace",value);
       		return augmentedDOM;
       	}
        </script>
    </body>
</html>
